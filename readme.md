# 项目申请书



[toc]



## 项目名称

基于MindSpore的量化感知训练，扩展混合bit量化功能



## 项目详细方案

有关混合位宽量化的问题，无论是训练后量化还是量化感知训练，最主要的问题都在量化位宽选取上。对于需要量化的层数为$L$的神经网络，可选量化位宽如${4, 8}$，则混合位宽的空间为$2^L$，难以全部尝试。

另一个问题是训练的问题，通常的量化感知训练方法是在前向计算时使用（伪）量化算子，模拟量化时的前向计算，在反向计算时按正常方法更新权值，这样训练更新的部分只有权值。也有将量化参数也参与训练的，效果好于只训练权值的量化感知训练[1]。

其他问题还有一些增加精度的手段等，比如部分训练，这样需要决定训练的顺序；量化的方法(scheme)，如对于量化截断值的选取方法，分通道，偏置修正等。



### 混合量化位宽选取方案

混合量化位宽选取是最核心的功能。根据了解的现有的一些方法，给出下面几种可能的方案。



#### 基于训练后量化敏感度的选取

在训练后量化中，一个网络中不同层对于量化产生的影响不同，即这个层的量化敏感度。量化对于输出产生的影响（误差）较大，即量化敏感度较大，通常需要较高的位宽。量化敏感度用于混合位宽训练后量化效果较好[2]，量化敏感度和数据分布和层位置关系较大。

猜想量化敏感度也可以用于量化感知训练中，因为量化感知训练使用浮点训好的权值，在后期微调中数据分布变化不大。即根据训练后量化的量化敏感度和层相关信息（参数量，运算量）决定量化位宽，最后进行量化感知训练。

算法流程

```python
# input: fp32model, bitOption
# output: intMixmodel
def mixQAT(fp32model, bitOption)
    intmodel = postTrainingQuantization(fp32model)
    quantizationSensetivity = evalQuantizationSensetivity(intmodel, bitOption)
    bits = solve(bitOption, quantizationSensetivity)
    mixedQuantModel = quantize(fp32model, bits)
    QAT(mixedQuantModel)
    return mixedQuantModel
```

这个方案的优点是计算量很小，在训练前就决定了位宽。缺点是最终精度结果不太能保证。



#### 基于搜索方案的选取

AutoML的方法如神经结构搜索(NAS)在网络结构设计上取得很好的效果，在其他问题搜索上也开始借鉴NAS的方法。但对于混合位宽量化的方法难以直接使用，因为搜索空间很大，并且每一个位宽配置的精度都需要训练（微调）过后才知道精度[3]。

一种可行的基于搜索的方法是[4]。原文包括量化和剪枝两部分，这里只使用其量化的部分。

![apq](img/apq.png)

这个方案的优点是，可以获得非常优的配置，可以在训练前预测其精度，最终精度结果比较能保证。缺点是需要的机时还是比较多的。



#### 基于一些统计量的选取

还有一些基于统计量的位宽选取方法，比如使用海森(Hessian)[5]，[6]，使用一些误差等统计量决定位宽[7]。

这样的方法优点是计算量也比较小（对于搜索来说），缺点是有些统计量数学计算比较麻烦，而且需要框架支持；有些统计量包含一些超参需要手工试。最终精度结果不太能保证。



### 训练(微调)方案

#### 基本的量化感知训练

前向计算时使用（伪）量化算子，模拟量化时的前向计算，在反向计算时按正常方法更新权值。



#### 量化参数可训练

使用PACT方法或者其改进。

前传计算
$$
y=0.5(|x|-|x-\alpha|+\alpha)\\
y_q=round(y\frac{2^k-1}{\alpha})\frac{\alpha}{2^k-1}
$$
反向计算使用STE，或改进的STE[8]。

不过这样的量化方法(scheme)需要框架支持，以及需要考虑部署的情况。

再如LSQ+的方法[9]，还考虑了初始化的问题，不过我看mindspore仓库master已经有人写了这个。

#### 可以部分训练（微调）

如果可以做部分训练，则可以在量化感知训练的时候决定按一定顺序训部分，这样更容易收敛和提高精度，避免位宽过低时精度归零的现象。缺点是可能需要训练代数增加。

微调顺序的决定，参考[6]。或者按序微调（也比全部一起训好）。



## 开发时间计划

* 7.1 - 7.10 搭建环境，数据集，浮点模型，运行全8位量化感知训练，查阅相关api支持情况，决定位宽选取方案。

* 7.10 - 7.25 开发位宽选取部分，比如量化敏感度求解器，APQ框架或者HAWQ框架
* 7.25 - 8.25 训练mobilenet v2，调试相关参数，达到项目效果
* 8.26 - 9.20 进一步优化，增加一些技巧等，增加一些额外功能；一些其他的网络
* 9.21 - 9.30 代码规范，整理结果



## 参考文献

[1] Choi J ,  Zhuo W ,  Venkataramani S , et al. PACT: Parameterized Clipping Activation for Quantized Neural Networks[J].  2018.

[2] Cai Y ,  Yao Z ,  Dong Z , et al. ZeroQ: A Novel Zero Shot Quantization Framework[J]. 2020 IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR), 2020.

[3] Kuan Wang, Zhijian Liu, Yujun Lin, Ji Lin, and Song Han.
Haq: Hardware-aware automated quantization. In CVPR, 2019.

[4] Wang T ,  Wang K ,  Cai H , et al. APQ: Joint Search for Network Architecture, Pruning and Quantization Policy[C]// 2020 IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR). IEEE, 2020.

[5] Dong Z ,  Yao Z ,  Gholami A , et al. HAWQ: Hessian AWare Quantization of Neural Networks with Mixed-Precision[C]// IEEE. IEEE, 2019.

[6] Dong Z ,  Yao Z ,  Y  Cai, et al. HAWQ-V2: Hessian Aware trace-Weighted Quantization of Neural Networks[J].  2019.

[7] Zhang X ,  Liu S ,  Zhang R , et al. Fixed-Point Back-Propagation Training[C]// 2020 IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR). IEEE, 2020.

[8] Zhang W ,  Hou L ,  Yin Y , et al. TernaryBERT: Distillation-aware Ultra-low Bit BERT[C]// 2020.


